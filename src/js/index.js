import $ from 'jquery'
window.jQuery = $;
window.$ = $;


import Swiper from 'swiper/swiper-bundle.cjs'
import select2 from 'select2/dist/js/select2'
/*
ON LOAD PAGE FUNCTION
*/

jQuery( window ).on( 'load', function() {

    $('body').removeClass('is-load');

} );

/*
INITIALIZATION FUNCTIONS
*/

jQuery( document ).ready( function( $ ) {
    
    $('select').select2()

    $('.lange-list').on('click', function (){
        $(this).toggleClass('is-active')
    })
    $('.lange-list span:not("is-active")').on('click', function (){
        $('.lange-list span').removeClass('is-active')
        $(this).addClass('is-active')
    })
    $('.drop-menu').on('click', function (){
        $(this).toggleClass('is-active')
    })

    $('.question--content--box li').on('click', function (){
        $('.question--content--box li').removeClass('is-active')
        $(this).addClass('is-active')
    })

    $('ul.list').on('click', 'li:not(.is-active)', function() {
        $(this)
            .addClass('is-active').siblings().removeClass('is-active')
            .closest('main').find('div.tab-content').removeClass('is-active').eq($(this).index()).addClass('is-active');
    });



   new Swiper('.index-banner--slider', {
        slidesPerView: 1,
        pagination: {
            el: '.index-banner--slider_pagination',
            type: 'bullets',
        },
    })




    function handleFileSelect(evt) {
        let files = evt.target.files;
        for (let i = 0, f; f = files[i]; i++) {
            if (!f.type.match('image.*')) {
                continue;
            }
            let reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e) {
                    let span = document.createElement('span');
                    span.innerHTML = ['<img class="thumb" src="', e.target.result,
                        '" title="', escape(theFile.name), '"/>'].join('');
                    document.getElementById('list').insertBefore(span, null);
                };
            })(f);
            reader.readAsDataURL(f);
        }
    }
    document.getElementById('files').addEventListener('change', handleFileSelect, false);

} );

/*
ON SCROLL PAGE FUNCTIONS
*/

jQuery( window ).on( 'scroll', function() {



} );